package pl.netlife.enigma;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Enigma2Activity extends Activity {
	
	String result;
    String startChoices;
    String text ;
    String t;
    TextView resultTV, runtimeTV;
    EditText textET;
    EnigmaEncryption enigmaSettings;
	BroadCastSender sender;
	BroadCastReceiver receiver;
	Button receiverButton, senderButton;

	Activity mContext;
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_enigma2);
        textET = (EditText) findViewById(R.id.toEncrypt);
        resultTV = (TextView) findViewById(R.id.text2); 
        runtimeTV = (TextView) findViewById(R.id.textView1);
		receiverButton = (Button) findViewById(R.id.button3);
		senderButton = (Button) findViewById(R.id.button4);
		
        Intent i  = getIntent();
     
        enigmaSettings = (EnigmaEncryption) i.getSerializableExtra("settings");
        
        
        startChoices = i.getExtras().getString("start_choices");
        
        
        
        
        
        
        
	}
	public void comeback(View view) {
		finish();
	}
	public void encrypt(View view){

		text = textET.getText().toString();


		if(text.matches("^[a-zA-Z ]+$")){
			text = text.replaceAll(" ", "X");
			text = text.toUpperCase();
			result=  enigmaSettings.encryptText(text, startChoices);
			resultTV.setText(result);
			//Toast myToast = Toast.makeText(this, "Time= "+t, Toast.LENGTH_SHORT);
			t =  ""+enigmaSettings.runtime+"ms";
			runtimeTV.setText(t);

		} else {
			textET.setError("Only latin letters and spaces!");
		}


	}
	

	
	public void send (View view) throws IOException{
	
		sender = new BroadCastSender(this.result, view);
		sender.execute();

	}
	
	public void receive(View view) throws IOException {
		receiver= new BroadCastReceiver(view,this);
		receiver.execute(textET);

		//textET.setText(receiver.getMessage());
		receiverButton.setEnabled(false);

		senderButton.setEnabled(true);
		
	}
	public void stop_receive(View view){

		receiver.stop();
		senderButton.setEnabled(false);
		//senderButton.setTextColor(Color.GRAY);
		receiverButton.setEnabled(true);
	}
	
}




