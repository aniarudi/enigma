package pl.netlife.enigma;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;



public class EnigmaEncryption implements Serializable {

	 
	
	String [] rotors;
	String transfer;
    String reflector;
    int [] ring;
    String plugBoardSettings;
    long runtime;
    
    public EnigmaEncryption(){
    	
    }
	
	public EnigmaEncryption(int ringSettings, String plugBoardState, int nbofReflector, int nbOfRotors,List <String> newRotor,int howManyItems) {
		rotors = new String[nbOfRotors];
		rotors[0] = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
		rotors[1] = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
		rotors[2] = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
		rotors[3] = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
		rotors[4] = "VZBRGITYUPSDNHLXAWMJQOFECK";
		int a=0;
		for(int i=nbOfRotors-howManyItems;i<nbOfRotors;i++) {
			if(howManyItems>0) {
				rotors[i]=newRotor.get(a);
				a++;
			}
		}
		
		String [] reflectors = new String[3];
		reflectors[0] ="EJMZALYXVBWFCRQUONTSPIKHGD";
		reflectors[1] = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
		reflectors[2] = "FVPJIAOYEDRZXWGCTKUQSBNMHL";
		reflector = reflectors[nbofReflector];
		transfer = "RFWKAXEMLHVBNZDQJTCIGPUOSY";
		ring = new int[3];
		char [] tmp;
        tmp = new char[plugBoardState.length()];
        plugBoardSettings = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
		for(int i = 2; i >= 0; i--) {
            ring[i] = (ringSettings % 10) - 1;
            ringSettings /= 10; 
        }
		tmp = plugBoardSettings.toCharArray();
        for(int i = 0; i < plugBoardState.length()-1; i += 2) {
            
            tmp[plugBoardState.charAt(i) - 65] = plugBoardState.charAt(i+1);
            tmp[plugBoardState.charAt(i+1) - 65] = plugBoardState.charAt(i);
        }
        plugBoardSettings = String.valueOf(tmp);	
	}
	
	public String encryptText(String textToEncrypt, String ringStartPosition) {
		long startTime = System.nanoTime();
		int j;
		int k=0;
		int n;
		int tmp, tmp2;
		char [] temp2;
		char [] temp;
		boolean movement;
		int textToEncryptChar;
		temp2 = new char[textToEncrypt.length()];
        temp = new char[textToEncrypt.length()];
		for(int i = 0;i<textToEncrypt.length(); i++) {
			 for(movement = true, j = 2; movement && (j >= 0); j--) {
				 tmp = ring[j];
	             if(ringStartPosition.charAt(j) == transfer.charAt(tmp)) {
	                  movement = true;
	                  tmp2 = ringStartPosition.charAt(j);
	                  temp2 = ringStartPosition.toCharArray();
	                  temp2[j] = (char)(65 + (tmp2 - 64)%26);
	                  ringStartPosition = String.valueOf(temp2);
	              }
	              else {
	            	  movement = false;
	                  tmp2 = ringStartPosition.charAt(j);
	                  temp2 = ringStartPosition.toCharArray();
	                  temp2[j] = (char)(65 + (tmp2 - 64)%26);
	                  ringStartPosition = String.valueOf(temp2);
	              }
			 }
			 textToEncryptChar = textToEncrypt.charAt(i);	
			 textToEncryptChar = plugBoardSettings.charAt(textToEncryptChar - 65);
			 for(j = 2; j >= 0; j--) {
				 k = (int)ringStartPosition.charAt(j) - 65;
	             textToEncryptChar = rotors[ring[j]].charAt((textToEncryptChar - 65 + k) % 26);
	             textToEncryptChar = 65 + (textToEncryptChar - 39 - k) % 26;
	         }
			 textToEncryptChar = reflector.charAt(textToEncryptChar - 65);
			 for(j = 0; j < 3; j++) {
	             k = ringStartPosition.charAt(j) - 65;
	             textToEncryptChar = 65 + (textToEncryptChar  - 65 + k) % 26;
	             for(n = 0; rotors[ring[j]].charAt(n) != textToEncryptChar; n++);
	             textToEncryptChar  = 65 + (26 + n - k) % 26;
	         } 
			 textToEncryptChar= plugBoardSettings.charAt(textToEncryptChar - 65);
			 temp = textToEncrypt.toCharArray();
	         temp[i] = (char)textToEncryptChar;
	         textToEncrypt = String.valueOf(temp);
		}
		this.runtime = (System.nanoTime()-startTime)/1000;
		return textToEncrypt;
		
	}
	
	public long getRuntime() {
		return this.runtime;
	}


	


	

}
