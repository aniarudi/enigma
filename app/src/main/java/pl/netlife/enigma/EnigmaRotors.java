package pl.netlife.enigma;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

public class EnigmaRotors extends Activity {
	
	String rotor;
	protected void onCreate(Bundle savedInstanceState) {	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotors);
        
		
	}
	public void addRotor(View v) {
		EditText value = (EditText) findViewById(R.id.typedRotor);
		rotor = value.getText().toString();
			
    	int check=-1;
		if(rotor.length() == 26) {
			check=0;		
			for(int i=0;i<rotor.length();i++) {
				for(int j=0;j<rotor.length();j++) {
					if(i!=j && rotor.charAt(i) == rotor.charAt(j)) {
						Toast myToast = Toast.makeText(this, "Letters cannot be repeated!", Toast.LENGTH_SHORT);
				    	myToast.setGravity(Gravity.CENTER, 0, 0);
				    	myToast.show();
				    	check++;
					}
				}		
			}
		}
		else {
			Toast myToast2 = Toast.makeText(this, "Rotor contains 26 letters!", Toast.LENGTH_SHORT);
	    	myToast2.setGravity(Gravity.CENTER, 0, 0);
	    	myToast2.show();
		}
		
		if(check == 0) {
        	
			Intent returnIntent = new Intent();
			returnIntent.putExtra("result",rotor);
			setResult(RESULT_OK,returnIntent);
			finish();
		}
	
	}

	public void comeback(View view) {
		finish();
	}
}
