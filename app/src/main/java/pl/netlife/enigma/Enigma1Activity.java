package pl.netlife.enigma;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Enigma1Activity extends Activity{
	
	ArrayAdapter<String> spinner2Adapter, spinner3Adapter, spinner1Adapter;  
	Spinner spinner1, spinner2, spinner3;
	String[] firstRotorArray;
	List<String> secondRotorArray, thirdRotorArray;	
	int nbOfRotors=5;
	List <String> newRotor=new ArrayList<String>(); //warto�ci dodanych rotor�w
	List <String> allRotors=new ArrayList<String>(); //numery rotor�w do spinner'a
	EnigmaEncryption enigmaSettings;
	int howMany=0; //ile dodanych
	Intent enigma1;
	private EditText stateChoice1;
	private EditText stateChoice2;
	private EditText stateChoice3;
	private EditText plug1;
	private EditText plug2;
	private EditText plug3;
	private EditText plug4;
	private EditText plug5;
	private EditText plug6;
	private EditText plug7;
	private EditText plug8;
	private EditText plug9;
	private EditText plug10;
	private Spinner mySpinner1;
	private Spinner mySpinner2;
	private Spinner mySpinner3;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enigma);
		enigma1 = new Intent(this, Enigma1Activity.class);
    	
		spinner1 = (Spinner)findViewById(R.id.firstRotor);
		spinner2 = (Spinner)findViewById(R.id.secondRotor);
		spinner3 = (Spinner)findViewById(R.id.thirdRotor);
		firstRotorArray = getResources().getStringArray(R.array.firstRotor);
		secondRotorArray = new ArrayList<String>();
		thirdRotorArray = new ArrayList<String>();
		stateChoice1 = (EditText) findViewById(R.id.stateChoice1);
		stateChoice2 = (EditText) findViewById(R.id.stateChoice2);
		stateChoice3 = (EditText) findViewById(R.id.stateChoice3);
		plug1 = (EditText) findViewById(R.id.plugBoard1);
		plug2 = (EditText) findViewById(R.id.plugBoard2);
		plug3 = (EditText) findViewById(R.id.plugBoard3);
		plug4 = (EditText) findViewById(R.id.plugBoard4);
		plug5 = (EditText) findViewById(R.id.plugBoard5);
		plug6 = (EditText) findViewById(R.id.plugBoard6);
		plug7 = (EditText) findViewById(R.id.plugBoard7);
		plug8 = (EditText) findViewById(R.id.plugBoard8);
		plug9 = (EditText) findViewById(R.id.plugBoard9);
		plug10 = (EditText) findViewById(R.id.plugBoard10);
		//ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.firstRotor, android.R.layout.simple_spinner_dropdown_item);
		

		stateChoice1.addTextChangedListener(new MyTextWatcher(stateChoice2));
		stateChoice2.addTextChangedListener(new MyTextWatcher(stateChoice3));
		stateChoice3.addTextChangedListener(new MyTextWatcher(plug1));
		plug1.addTextChangedListener(new MyTextWatcher(plug6));
		plug2.addTextChangedListener(new MyTextWatcher(plug7));
		plug3.addTextChangedListener(new MyTextWatcher(plug8));
		plug4.addTextChangedListener(new MyTextWatcher(plug9));
		plug5.addTextChangedListener(new MyTextWatcher(plug10));
		plug6.addTextChangedListener(new MyTextWatcher(plug2));
		plug7.addTextChangedListener(new MyTextWatcher(plug3));
		plug8.addTextChangedListener(new MyTextWatcher(plug4));
		plug9.addTextChangedListener(new MyTextWatcher(plug5));



		for(int i=0;i<nbOfRotors;i++) { 
			allRotors.add(Integer.toString(i+1)); 
		}
		//adapter do pierwszego spinner'a, aby dodawal wartosci dodane do listy allRotors
        spinner1Adapter =new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, allRotors);
		spinner1Adapter .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner1.setAdapter(spinner1Adapter);

		
		for(int i=0;i<nbOfRotors;i++)
			firstRotorArray[i] = allRotors.get(i).toString(); //nadpisuj�, �eby dzia�a�o dodawanie kolejnych cyferek
		
		spinner2Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, secondRotorArray);
		spinner2Adapter .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(spinner2Adapter);
        
          
        spinner3Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, thirdRotorArray);
        spinner3Adapter .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner3.setAdapter(spinner3Adapter);
		
		
		spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	secondRotorArray.clear();
                for(int j = 0;j< nbOfRotors; j++) {
                	if( i != j ) {
                		secondRotorArray.add(allRotors.get(j).toString());
                	}
                }
                spinner2Adapter.notifyDataSetChanged();
            } 

            public void onNothingSelected(AdapterView<?> adapterView) {
            	return;
            } 
        });
		//zakomentowane poni�ej to co by�o przed modyfikacj�
		/*spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	secondRotorArray.clear();
                for(int j = 0;j< firstRotorArray.length; j++) {           	
                	if( i != j ) {
                		secondRotorArray.add(firstRotorArray[j]);
                	}
                }
                spinner2Adapter.notifyDataSetChanged();
            } 

            public void onNothingSelected(AdapterView<?> adapterView) {
            	return;
            } 
        });*/
        
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            	thirdRotorArray.clear();
                for(int j = 0;j< secondRotorArray.size(); j++) {
                	if( i != j ) {
                		thirdRotorArray.add(secondRotorArray.get(j));
                	}
                }
                spinner3Adapter.notifyDataSetChanged();
            } 

            public void onNothingSelected(AdapterView<?> adapterView) {
            	return;
            } 
        });

 
        spinner1.setSelection(0);
        spinner2.setSelection(0);
        spinner3.setSelection(0);
        }


	
	public void verify(View view) {
		int check=0;

		String oneString= stateChoice1.getText().toString();
		if(!oneString.matches("^[a-zA-Z]$")) {
			stateChoice1.setError(Html.fromHtml("<font color='red'>Letters only!</font>"));
			check++;
		}


		String twoString= stateChoice2.getText().toString();
		if(!twoString.matches("^[a-zA-Z]$")) {
			stateChoice2.setError("Letters only!");
			check++;
		}

		String threeString= stateChoice3.getText().toString();
		if(!threeString.matches("^[a-zA-Z]$")) {
			stateChoice3.setError(" Letters only! ");
			check++;
		}
		String startChoices = oneString+twoString+threeString; 
		startChoices = startChoices.toUpperCase();



		String plugB1 = plug1.getText().toString();
		String plugB2 = plug2.getText().toString();
		String plugB3 = plug3.getText().toString();
		String plugB4 = plug4.getText().toString();
		String plugB5 = plug5.getText().toString();
		String plugB6 = plug6.getText().toString();
		String plugB7 = plug7.getText().toString();
		String plugB8 = plug8.getText().toString();
		String plugB9 = plug9.getText().toString();
		String plugB10 = plug10.getText().toString();
		String plugs = plugB1+plugB2+plugB3+plugB4+plugB5+plugB6+plugB7+plugB8+plugB9+plugB10;

		mySpinner1 = (Spinner) findViewById(R.id.firstRotor);
		String rotor1 = mySpinner1.getSelectedItem().toString();
		mySpinner2 = (Spinner) findViewById(R.id.secondRotor);
		String rotor2 = mySpinner2.getSelectedItem().toString();
		mySpinner3 = (Spinner) findViewById(R.id.thirdRotor);
		String rotor3 = mySpinner3.getSelectedItem().toString();
		String rotorsChoices = rotor1+rotor2+rotor3;
		int rotors = Integer.parseInt(rotorsChoices);
		//TextView testing = (TextView) findViewById(R.id.test);
		//testing.setText(rotors+"\n");		
		
	
		Spinner whichReflector=(Spinner) findViewById(R.id.reflectorChoice);
		int reflector = (int) whichReflector.getSelectedItemId();
		//TextView testing = (TextView) findViewById(R.id.test);
		//testing.setText(reflector+"\n");

		for(int i=0;i<plugs.length();i++) {
			for(int j=0;j<plugs.length();j++) {
				if(i!=j && plugs.charAt(i) == plugs.charAt(j)) {
					Toast myToast = Toast.makeText(this, "Letters cannot be repeated!", Toast.LENGTH_SHORT);
			    	myToast.setGravity(Gravity.CENTER, 0, 0);
			    	myToast.show();
			    	check++;
				}
			    		
			}
		}		

    		if(oneString.isEmpty()|| twoString.isEmpty()|| threeString.isEmpty()) {
    			check++;
    			Toast myToast11 = Toast.makeText(this, "Each rotor needs starting letter", Toast.LENGTH_SHORT);
		    	myToast11.setGravity(Gravity.CENTER, 0, 0);
		    	myToast11.show();
    		}
    		
    				
    	
		if(check ==0) {
			
			enigmaSettings = new EnigmaEncryption( rotors, plugs, reflector,nbOfRotors,newRotor,howMany);	
			Intent myIntent = new Intent(this, Enigma2Activity.class);
			myIntent.putExtra("settings",enigmaSettings);
			myIntent.putExtra("start_choices", startChoices);
			
			
			startActivity(myIntent);
		}
	
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	    if (requestCode == 1) {
	        if(resultCode == RESULT_OK){
	            String result=data.getStringExtra("result");
	            newRotor.add(result);
	            nbOfRotors++;
	            allRotors.add(Integer.toString(nbOfRotors));
	            howMany++;
	            //dodaje nowe warto�ci do drugiego i trzeciego spinnera
	            secondRotorArray.add(Integer.toString(nbOfRotors));
	            thirdRotorArray.add(Integer.toString(nbOfRotors));
	            Toast myToast9 = Toast.makeText(this, "all rotors "+allRotors+" nbOfRotrs "+nbOfRotors+" "+firstRotorArray.length, Toast.LENGTH_SHORT);
	    		myToast9.setGravity(Gravity.CENTER, 0, 0);
	        	myToast9.show();

	            
	        }
	        if (resultCode == RESULT_CANCELED) {
	            Toast myToast9 = Toast.makeText(this, "No new rotor added", Toast.LENGTH_SHORT);
	    		myToast9.setGravity(Gravity.CENTER, 0, 0);
	        	myToast9.show();
	        }
	    }
	}
	public void generateRotorValues(View view) {
		if(howMany<20) {
			Intent myIntent = new Intent(this, EnigmaRotors.class);
			startActivityForResult(myIntent, 1);
		}
		else {
			 Toast myToast9 = Toast.makeText(this, "Rotors limit is 26!", Toast.LENGTH_SHORT);
			 myToast9.setGravity(Gravity.CENTER, 0, 0);
	         myToast9.show();
		}
		
		
	}

}
