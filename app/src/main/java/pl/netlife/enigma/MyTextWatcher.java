package pl.netlife.enigma;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

/**
 * Created by rudi on 26.11.17.
 */

public class MyTextWatcher implements TextWatcher{
    private final EditText nextEditText;
    int MAX_LENGTH = 1;

    public MyTextWatcher(EditText et){
        nextEditText = et;
    }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        if(charSequence.length() == MAX_LENGTH){
            nextEditText.requestFocus(View.FOCUS_FORWARD);
        }

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
