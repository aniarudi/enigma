package pl.netlife.enigma;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.view.View;

import java.net.SocketException;

import android.app.Activity;

import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BroadCastReceiver extends AsyncTask <EditText, Integer, String>{
	private static final int PORT = 2562;

	private Context mContext;
	protected View view;
	boolean stop;
	EditText editText;
	Button listen, stopListen;
	Enigma2Activity activity;

	public BroadCastReceiver(View view, Enigma2Activity activity){
		
		this.view = view;
		stop = false;
		this.activity = activity;
		Log.i("Receiver", "receiver created");
		listen = (Button) this.view.findViewById(R.id.button3);
		stopListen = (Button) view.findViewById(R.id.button4);


	}
	InetAddress getBroadcastAddress() throws IOException {
		mContext = this.view.getContext();
	    WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
	    DhcpInfo dhcp = wifi.getDhcpInfo();
	    // handle null somehow

	    int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
	    byte[] quads = new byte[4];
	    for (int k = 0; k < 4; k++)
	      quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
	    return InetAddress.getByAddress(quads);
	}
	public String receive() throws IOException{
		DatagramSocket socket = new DatagramSocket(PORT);
		socket.setBroadcast(true);
		byte[] buf = new byte[1024];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		Log.i("Receiver", "message received");
		socket.receive(packet);
		
		socket.close();


		return new String(buf, 0, packet.getLength());
		
	}
	

	
		
	/*public String getMessage(){
		return message;
	}*/
	public void stop(){
		this.stop = true;
		Log.i("Receiver", "receiver stopped");

	}
	
	@Override
	protected String doInBackground(EditText... params) {

		if(params.length != 1){
			return null;
		}

		editText = params[0];
		try {
			while(!stop){
				String message = this.receive();
				Log.i("Receiver", "message: "+message);
				return message;

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		return null;
	}
	
	@Override protected void onPostExecute(String result){

		editText.setText(result);

		activity.stop_receive(view);
	}

}


