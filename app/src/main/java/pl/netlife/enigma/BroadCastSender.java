package pl.netlife.enigma;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.view.View;

import java.net.SocketException;

import android.app.Activity;

import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BroadCastSender extends AsyncTask {
	private static final int PORT = 2562;
	protected String message;
	private Context mContext;
	protected View view;


	public BroadCastSender(String text, View view){
		message = text;
		this.view = view;
	}
	InetAddress getBroadcastAddress() throws IOException {
		mContext = this.view.getContext();
	    WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
	    DhcpInfo dhcp = wifi.getDhcpInfo();
	    // handle null somehow

	    int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
	    byte[] quads = new byte[4];
	    for (int k = 0; k < 4; k++)
	      quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
	    return InetAddress.getByAddress(quads);
	}
	public void send() throws IOException{
		Log.i("Sender", "message in progress");
		DatagramSocket socket = new DatagramSocket(PORT);
		socket.setBroadcast(true);
		String data = message;
		//Log.d(TAG, "Sending data " + data);

		DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(),
		    getBroadcastAddress(),PORT);
		socket.send(packet);
		Log.i("Sender", "message sent");
		socket.close();

		
	}
			
	@Override
	protected Object doInBackground(Object... params) {
	
		try {
			this.send();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		return null;
	}
	
	protected void onPostExecuted(){


		
	}

}
